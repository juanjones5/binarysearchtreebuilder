package bstbuilder;
import java.util.Stack;

public class BST<T> {

    int preIndex; //To keep track of the index in the preOrder list while doing the recursion
    int size; // # of nodes in the tree

    // simple constructor with the size
    public BST(int size){
        this.size = size;
    }

    // Builds the tree recursively
    public Node<T> buildTree(T preOrder[], T inOrder[], int first, int last){

        if (preIndex == size || first > last) return null;
        Node root = new Node(preOrder[preIndex]);

        int rootIndex;
        for (rootIndex = first; rootIndex <= last; rootIndex++) {
            if (inOrder[rootIndex] == preOrder[preIndex]) break;
        }

        this.preIndex ++;
        if(first == last) return root;

        if(rootIndex > first)
            root.left = buildTree(preOrder, inOrder, first, rootIndex-1);
        if(rootIndex < last)
            root.right = buildTree(preOrder, inOrder, rootIndex+1, last);

        return root;
    }


    // Calculates the height of the tree recursively
    public int height (Node root) {
        if (root == null) return 0;
        int leftHeight = height (root.left);
        int rightHeight = height (root.right);

        if (leftHeight > rightHeight)
            return (leftHeight + 1);
        else
            return (rightHeight + 1);
    }

    // To print level by level
    public void levelTraversal (Node root){
        int treeHeight = height(root);
        System.out.println("By level traversal with Level number: ");
        for (int i=0; i<treeHeight; i++) {
            System.out.print("Level " + i + ": ");
            levelTraversalHelper(root, i);
            System.out.println();
        }

    }

    // helper function to print each level
    public void levelTraversalHelper (Node root, int level){
        if (root==null) return;
        if (level == 0)
            System.out.print(root.data + " ");
        else if (level>0) {
            levelTraversalHelper(root.left, level - 1);
            levelTraversalHelper(root.right, level - 1);
        }

    }

    // Non recursive function to print the postorder, it uses a stack instead
    public void printPostOrder (Node node){
        if (node == null) return;
        System.out.println("Non-recursive postorder traversal:");

        Stack<Node> stackN = new Stack<>();
        while (node != null || !stackN.isEmpty()) {
            while (node != null) {
                stackN.push(node);
                stackN.push(node);
                node = node.left;
            }
            if (stackN.isEmpty()) return;
            node = stackN.pop();
            if (!stackN.isEmpty() && stackN.peek() == node)
                node = node.right;
            else {
                System.out.print(node.data + " ");
                node = null;
            }
        }
        System.out.println();
    }

    // Non recursive function to print the inorder, it uses a stack instead
    public void printInOrder (Node node){
        if (node == null) return;
        System.out.println("Non-recursive inorder traversal:");

        Stack<Node> stackN = new Stack<>();

        while (node != null || !stackN.isEmpty()) {
            while (node != null) {
                stackN.push(node);
                node = node.left;
            }
            node = stackN.pop();
            System.out.print(node.data + " ");
            node = node.right;
        }
        System.out.println();
    }

    // Non recursive function to print the preorder, it uses a stack instead
    public void printPreOrder (Node node){
        if (node == null) return;

        System.out.println("Non-recursive preorder traversal:");

        Stack<Node> stackN = new Stack<>();
        stackN.push(node);

        while (!stackN.isEmpty()) {
            Node temp = stackN.pop();
            System.out.print(temp.data + " ");
            if (temp.right != null)
                stackN.push(temp.right);
            if (temp.left != null)
                stackN.push(temp.left);
        }
        System.out.println();
    }
}
