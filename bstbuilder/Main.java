package bstbuilder;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {

    public static void main(String[] args) throws IOException {

        // The program starts with a demo like the one showed in the homework instructions
        System.out.println("This is an example of how the program works:");
        System.out.println("For the inorder sequence 2 5 6 10 12 14 15");
        System.out.println("And the preorder sequence 10 5 2 6 14 12 15");

        Integer[] inOrderExample = {2, 5, 6, 10, 12, 14, 15};
        Integer[] preOrderExample = {10, 5, 2, 6, 14, 12, 15};

        int size = preOrderExample.length;

        BST<Integer> tree = new BST<>(size); //initializes a tree

        Node root = tree.buildTree(preOrderExample, inOrderExample, 0, size); //builds the tree
        System.out.println("We get the following tree:");
        //Calling all the required methods
        System.out.println();
        tree.printInOrder(root);
        System.out.println();
        tree.printPreOrder(root);
        System.out.println();
        tree.printPostOrder(root);
        System.out.println();
        tree.levelTraversal(root);
        System.out.println();

        System.out.println("**********************************");
        System.out.println();

        //In this section the user can run tests
        while (true) {
            System.out.println();
            System.out.println("Try now with your own BST, press Y to continue or Q to exit"); // The user can exit the program here
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
            String inputString = inputReader.readLine();
            if (inputString.equals("q") || inputString.equals("Q")){
                break;
            }

            //in order to avoid using ArrayList the user needs to specify the size of the tree that is going to test
            while (true) {
                System.out.println("First, specify how many integers will be in your tree");
                try {
                    inputString = inputReader.readLine();

                    size = Integer.parseInt(inputString);
                    break;
                }
                catch (NumberFormatException error){
                    System.out.println("Please enter an integer"); //if the input is not an integer, the program will ask again for an integer
                    continue;
                }
            }

            // the arrays that will be filled with the user input
            Integer[] inOrder = new Integer[size];
            Integer[] preOrder = new Integer[size];

            // The program ask for the inorder sequence of integers and does error checking
            while (true) {
                System.out.println("Enter the inorder sequence of integers with spaces in between");
                System.out.println("For example: 2 5 6 10 12 14 15");
                inputString = inputReader.readLine();
                String[] input = inputString.split(" "); // to split the input by spaces
                if (input.length != size) {
                    System.out.println("You did not respect the specified tree size");
                    continue;
                }
                try {
                    int i = 0;
                    for (String s : input) {
                        inOrder[i] = Integer.parseInt(s);
                        i++;
                    }
                    break;
                }
                catch (NumberFormatException error){
                    System.out.println("Please enter only integers");
                    continue;
                }
            }

            // The program ask for the preorder sequence of integers and does error checking
            while (true){
                System.out.println("Enter the preorder sequence of integers with spaces in between");
                System.out.println("For example: 10 5 2 6 14 12 15");
                inputString = inputReader.readLine();
                String[] input = inputString.split(" ");
                if (input.length != size) {
                    System.out.println("You did not respect the specified tree size");
                    continue;
                }
                try {
                    int i = 0;
                    for (String s : input) {
                        preOrder[i] = Integer.parseInt(s);
                        i++;
                    }
                    break;
                }
                catch (NumberFormatException error){
                    System.out.println("Please enter only integers");
                    continue;
                }
            }

            //new BST
            BST <Integer> newtree = new BST<>(size);

            //Building the bst and calling all the required methods
            root = newtree.buildTree(preOrder, inOrder, 0, size);
            System.out.println("We get the following tree:");
            System.out.println();
            newtree.printInOrder(root);
            System.out.println();
            newtree.printPreOrder(root);
            System.out.println();
            newtree.printPostOrder(root);
            System.out.println();
            newtree.levelTraversal(root);
            System.out.println();
        }

    }
}
